package com.httpain.meatballs.infrastructure.integration.sparkpost;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SparkpostModule {

    @Bean
    SparkpostApi sparkpostApi(@Value("${sparkpost.baseUrl}") String baseUrl) {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(SparkpostApi.class, baseUrl);
    }

}
