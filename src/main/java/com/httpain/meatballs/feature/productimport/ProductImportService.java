package com.httpain.meatballs.feature.productimport;

import com.httpain.meatballs.infrastructure.integration.ikea.IkeaClient;
import com.httpain.meatballs.infrastructure.storage.ProductRepository;
import com.httpain.meatballs.model.Location;
import com.httpain.meatballs.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductImportService {

    private final IkeaClient ikeaClient;
    private final ProductRepository productRepository;

    public ProductImportService(IkeaClient ikeaClient,
                                ProductRepository productRepository) {
        this.ikeaClient = ikeaClient;
        this.productRepository = productRepository;
    }

    public void parseNewProducts(Location location) {
        List<Product> newestProductsForLocation = ikeaClient.parseLatestProducts(location);
        productRepository.addAll(newestProductsForLocation);
    }
}
