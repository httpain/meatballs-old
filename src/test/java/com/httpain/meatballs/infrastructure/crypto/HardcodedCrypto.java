package com.httpain.meatballs.infrastructure.crypto;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

@Configuration
public class HardcodedCrypto {

    private static final PasswordEncoder subscribeTokenEncoder = newEncoder();

    private static final PasswordEncoder unsubscribeTokenEncoder = newEncoder();

    private static final TextEncryptor identityEncryptor = newEncryptor();

    @Bean
    @Qualifier("subscribeTokenEncoder")
    public PasswordEncoder subscribeTokenEncoder() {
        return subscribeTokenEncoder;
    }

    @Bean
    @Qualifier("unsubscribeTokenEncoder")
    public PasswordEncoder unsubscribeTokenEncoder() {
        return unsubscribeTokenEncoder;
    }

    @Bean
    @Qualifier("identityEncryptor")
    public TextEncryptor identityEncryptor() {
        return identityEncryptor;
    }

    private static PasswordEncoder newEncoder() {
        String secret = KeyGenerators.string().generateKey();
        return new Pbkdf2PasswordEncoder(secret);
    }

    private static TextEncryptor newEncryptor() {
        String password = new Base64StringKeyGenerator(32).generateKey();
        String hexSalt = KeyGenerators.string().generateKey();
        return Encryptors.text(password, hexSalt);
    }
}
