package com.httpain.meatballs.infrastructure.integration.sparkpost;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

interface SparkpostApi {

    @RequestLine("POST /transmissions/")
    @Headers("Authorization: {apiKey}")
    Response sendEmail(@Param("apiKey") String apiKey, Transmission transmission);

}
