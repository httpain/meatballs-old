package com.httpain.meatballs.admin;

import com.httpain.meatballs.feature.newsletter.NewsletterService;
import com.httpain.meatballs.infrastructure.storage.LocationRepository;
import com.httpain.meatballs.model.Location;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

@Endpoint(id = "triggerNewsletter")
@Component
public class NewsletterAdminTrigger {

    private final NewsletterService newsletterService;
    private final LocationRepository locationRepository;

    public NewsletterAdminTrigger(NewsletterService newsletterService,
                                  LocationRepository locationRepository) {
        this.newsletterService = newsletterService;
        this.locationRepository = locationRepository;
    }

    @WriteOperation
    public void triggerNewsletter(@Selector long locationId) {
        Location location = locationRepository.findById(locationId);
        newsletterService.sendNewsletterForLocation(location);
    }

}
