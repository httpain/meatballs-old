package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
class Options {
    @JsonProperty("open_tracking")
    boolean openTracking;
    @JsonProperty("click_tracking")
    boolean clickTracking;

    static Options noTracking() {
        return new Options(false, false);
    }
}
