package com.httpain.meatballs.infrastructure.storage;

import com.httpain.meatballs.model.Subscription;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class SubscriptionRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public SubscriptionRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void add(Subscription subscription) {
        //TODO does OR IGNORE make sense there
        jdbcTemplate.update("INSERT OR IGNORE " +
                        "INTO subscription (email, location_id) " +
                        "VALUES (:email, :locationId)",
                new BeanPropertySqlParameterSource(subscription));
    }

    public List<Subscription> findByLocation(long locationId) {
        return jdbcTemplate.query("SELECT email, location_id " +
                        "FROM subscription WHERE location_id = :locationId",
                new MapSqlParameterSource("locationId", locationId),
                new SubscriptionMapper()
        );
    }

    public boolean exists(Subscription subscription) {
        List<Subscription> result = jdbcTemplate.query("SELECT email, location_id " +
                        "FROM subscription " +
                        "WHERE email = :email AND location_id = :locationId",
                new BeanPropertySqlParameterSource(subscription),
                new SubscriptionMapper()
        );
        return result.size() > 0;
    }

    public void remove(Subscription subscription) {
        jdbcTemplate.update("DELETE FROM subscription " +
                        "WHERE email = :email AND location_id = :locationId",
                new BeanPropertySqlParameterSource(subscription));
    }

    private static class SubscriptionMapper implements RowMapper<Subscription> {
        @Override
        public Subscription mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Subscription(
                    rs.getString("email"),
                    rs.getLong("location_id")
            );
        }
    }


}
