package com.httpain.meatballs.model;

import lombok.Value;

@Value
public class Product {
    String name;
    String description;
    String price;
    String url;
    String imageUrl;
    long locationId;
}
