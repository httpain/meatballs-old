package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.httpain.meatballs.model.Email;
import com.httpain.meatballs.model.NewsletterSubscriber;

import java.util.List;

public interface EmailSenderClient {
    void sendTransactionalEmail(Email email, String recipientEmail);

    void sendNewsletterEmail(Email email, List<NewsletterSubscriber> subscribers);
}
