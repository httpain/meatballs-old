package com.httpain.meatballs.feature.subscription;

import com.httpain.meatballs.infrastructure.storage.LocationRepository;
import com.httpain.meatballs.model.Location;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexPageService {

    private final LocationRepository locationRepository;

    public IndexPageService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getLocations() {
        return locationRepository.findAll();
    }
}
