CREATE TABLE location (
  id             INTEGER PRIMARY KEY,
  name           TEXT UNIQUE,
  hostname       TEXT,
  new_items_path TEXT,
  UNIQUE (hostname, new_items_path)
);

CREATE TABLE product (
  id                     INTEGER PRIMARY KEY,
  name                   TEXT,
  description            TEXT,
  price                  TEXT,
  url               TEXT,
  image_url             TEXT,
  location_id            INTEGER,
  included_in_newsletter TEXT,
  UNIQUE (url, location_id),
  FOREIGN KEY (location_id) REFERENCES location (id)
);

CREATE TABLE subscription (
  id               INTEGER PRIMARY KEY,
  email TEXT,
  location_id      INTEGER,
  UNIQUE (email, location_id),
  FOREIGN KEY (location_id) REFERENCES location (id)
);
