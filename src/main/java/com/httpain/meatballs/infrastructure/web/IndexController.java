package com.httpain.meatballs.infrastructure.web;

import com.httpain.meatballs.feature.subscription.IndexPageService;
import com.httpain.meatballs.model.Location;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexController {

    private final IndexPageService indexPageService;

    public IndexController(IndexPageService indexPageService) {
        this.indexPageService = indexPageService;
    }

    @GetMapping("/")
    public String indexPage(Model model) {
        List<Location> locations = indexPageService.getLocations();
        model.addAttribute("locations", locations);
        return "index";
    }
}
