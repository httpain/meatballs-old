package com.httpain.meatballs.feature.subscription;

import com.httpain.meatballs.infrastructure.integration.EmailBuilder;
import com.httpain.meatballs.infrastructure.integration.sparkpost.EmailSenderClient;
import com.httpain.meatballs.infrastructure.storage.LocationRepository;
import com.httpain.meatballs.infrastructure.storage.SubscriptionRepository;
import com.httpain.meatballs.model.Email;
import com.httpain.meatballs.model.Location;
import com.httpain.meatballs.model.Subscription;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final EmailSenderClient emailSenderClient;
    private final TextEncryptor textEncryptor;
    private final PasswordEncoder tokenEncoder;
    private final EmailBuilder emailBuilder;
    private final LocationRepository locationRepository;

    public SubscriptionService(SubscriptionRepository subscriptionRepository,
                               EmailSenderClient emailSenderClient,
                               @Qualifier("identityEncryptor") TextEncryptor textEncryptor,
                               @Qualifier("subscribeTokenEncoder") PasswordEncoder tokenEncoder,
                               EmailBuilder emailBuilder,
                               LocationRepository locationRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.emailSenderClient = emailSenderClient;
        this.textEncryptor = textEncryptor;
        this.tokenEncoder = tokenEncoder;
        this.emailBuilder = emailBuilder;
        this.locationRepository = locationRepository;
    }

    public SubscriptionType sendConfirmation(String emailAddress, long locationId) {
        Subscription subscription = new Subscription(emailAddress, locationId);

        if (subscriptionRepository.exists(subscription)) {
            return SubscriptionType.EXISTS;
        }

        String identity = subscription.toIdentity();
        String encryptedIdentity = textEncryptor.encrypt(identity);
        String confirmationToken = tokenEncoder.encode(identity);

        String confirmationUrl = UriComponentsBuilder.newInstance()
                .scheme("http").host("localhost").port(8080)
                .path("/confirm")
                .queryParam("i", encryptedIdentity)
                .queryParam("t", confirmationToken)
                .build().toString();

        Location location = locationRepository.findById(locationId);

        Email email = emailBuilder.buildConfirmationEmail(location.getName(), confirmationUrl);
        emailSenderClient.sendTransactionalEmail(email, emailAddress);

        return SubscriptionType.NEW;
    }

    public SubscriptionType confirmSubscription(String encryptedIdentity, String token) {
        String identity = textEncryptor.decrypt(encryptedIdentity);
        if (!tokenEncoder.matches(identity, token)) {
            throw new RuntimeException("wrong link!");
        }
        Subscription subscription = Subscription.parseIdentity(identity);

        if (subscriptionRepository.exists(subscription)) {
            return SubscriptionType.EXISTS;
        } else {
            subscriptionRepository.add(subscription);
            return SubscriptionType.NEW;
        }
    }


    public void cancelSubscription(String encryptedIdentity, String token) {
        String identity = textEncryptor.decrypt(encryptedIdentity);
        if (tokenEncoder.matches(identity, token)) {
            subscriptionRepository.remove(Subscription.parseIdentity(identity));
        }
    }


    public enum SubscriptionType {
        NEW,
        EXISTS
    }
}
