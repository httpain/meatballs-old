package com.httpain.meatballs.infrastructure.storage;

import com.httpain.meatballs.model.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ProductRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public ProductRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addAll(List<Product> products) {
        jdbcTemplate.batchUpdate("INSERT OR IGNORE INTO " +
                        "product (name, description, price, url, image_url, location_id) \n" +
                        "VALUES (:name, :description, :price, :url, :imageUrl, :locationId)",
                products.stream()
                        .map(BeanPropertySqlParameterSource::new)
                        .toArray(SqlParameterSource[]::new));
    }

    public List<Product> findPending(long locationId) {
        return jdbcTemplate.query("SELECT name, description, price, url, image_url, location_id FROM product " +
                        "WHERE included_in_newsletter IS NULL AND location_id = :locationId",
                new MapSqlParameterSource("locationId", locationId),
                new ProductRowMapper());
    }

    public void markIncludedInNewsletter(Product product, String timestamp) {
        jdbcTemplate.update("UPDATE product SET included_in_newsletter = :timestamp " +
                        "WHERE location_id = :locationId AND url = :url",
                new MapSqlParameterSource("locationId", product.getLocationId())
                        .addValue("url", product.getUrl())
                        .addValue("timestamp", timestamp));
    }

    private static class ProductRowMapper implements RowMapper<Product> {
        @Override
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Product(
                    rs.getString("name"),
                    rs.getString("description"),
                    rs.getString("price"),
                    rs.getString("url"),
                    rs.getString("image_url"),
                    rs.getLong("location_id"));
        }
    }

}
