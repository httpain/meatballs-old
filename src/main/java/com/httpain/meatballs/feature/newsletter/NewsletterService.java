package com.httpain.meatballs.feature.newsletter;

import com.httpain.meatballs.infrastructure.integration.EmailBuilder;
import com.httpain.meatballs.infrastructure.integration.sparkpost.EmailSenderClient;
import com.httpain.meatballs.infrastructure.storage.ProductRepository;
import com.httpain.meatballs.infrastructure.storage.SubscriptionRepository;
import com.httpain.meatballs.model.Email;
import com.httpain.meatballs.model.Location;
import com.httpain.meatballs.model.NewsletterSubscriber;
import com.httpain.meatballs.model.Product;
import com.httpain.meatballs.model.Subscription;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NewsletterService {

    private final ProductRepository productRepository;
    private final SubscriptionRepository subscriptionRepository;
    private final EmailSenderClient emailSenderClient;
    private final TextEncryptor textEncryptor;
    private final PasswordEncoder tokenEncoder;
    private final EmailBuilder emailBuilder;


    public NewsletterService(ProductRepository productRepository,
                             SubscriptionRepository subscriptionRepository,
                             EmailSenderClient emailSenderClient,
                             @Qualifier("identityEncryptor") TextEncryptor textEncryptor,
                             @Qualifier("unsubscribeTokenEncoder") PasswordEncoder tokenEncoder,
                             EmailBuilder emailBuilder) {
        this.productRepository = productRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.emailSenderClient = emailSenderClient;
        this.textEncryptor = textEncryptor;
        this.tokenEncoder = tokenEncoder;
        this.emailBuilder = emailBuilder;
    }

    @Transactional
    public void sendNewsletterForLocation(Location location) { //TODO or accept locationIds in service layer?
        List<Product> pendingProducts = productRepository.findPending(location.getId());

        List<NewsletterSubscriber> newsletterSubscribers = subscriptionRepository.findByLocation(location.getId()).stream() //TODO subscription service should provide a list
                .map(subscription -> new NewsletterSubscriber(subscription.getEmail(), buildUrl(subscription)))
                .collect(Collectors.toList());

        Email email = emailBuilder.buildNewsletterEmail(location.getName(), pendingProducts);

        emailSenderClient.sendNewsletterEmail(email, newsletterSubscribers);

        Instant now = Instant.now();
        pendingProducts.forEach(product -> productRepository.markIncludedInNewsletter(product, now.toString()));
    }

    private String buildUrl(Subscription subscription) {
        String identity = subscription.toIdentity();
        String encryptedIdentity = textEncryptor.encrypt(identity);
        String confirmationToken = tokenEncoder.encode(identity);

        return UriComponentsBuilder.newInstance()
                .scheme("http").host("localhost").port(8080)
                .path("/unsubscribe")
                .queryParam("i", encryptedIdentity)
                .queryParam("t", confirmationToken)
                .build().toString();
    }
}
