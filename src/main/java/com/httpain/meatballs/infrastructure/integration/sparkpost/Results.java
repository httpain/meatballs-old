package com.httpain.meatballs.infrastructure.integration.sparkpost;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
class Results {
    @JsonCreator
    public Results(@JsonProperty("total_rejected_recipients") Integer totalRejectedRecipients,
                   @JsonProperty("total_accepted_recipients") Integer totalAcceptedRecipients,
                   @JsonProperty("id") String id) {
        this.totalRejectedRecipients = totalRejectedRecipients;
        this.totalAcceptedRecipients = totalAcceptedRecipients;
        this.id = id;
    }

    Integer totalRejectedRecipients;
    Integer totalAcceptedRecipients;
    String id;
}
