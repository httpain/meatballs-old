package com.httpain.meatballs.infrastructure.integration.sparkpost;

import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
class Transmission {
    Options options;
    List<Recipient> recipients;
    Content content;
}
