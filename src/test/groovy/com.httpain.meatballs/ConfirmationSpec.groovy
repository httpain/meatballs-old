package com.httpain.meatballs

import com.httpain.meatballs.infrastructure.crypto.HardcodedCrypto
import com.httpain.meatballs.model.Subscription
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification
import wiremock.org.apache.http.client.HttpClient
import wiremock.org.apache.http.client.methods.HttpGet
import wiremock.org.apache.http.impl.client.HttpClientBuilder

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = [Application.class, HardcodedCrypto.class])
@ActiveProfiles("test")
class ConfirmationSpec extends Specification {

    @LocalServerPort
    int port

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate

    // TestRestTemplate has redirects on by default and it's non-trivial to turn them off
    HttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build()

    HardcodedCrypto crypto = new HardcodedCrypto()

    Subscription subscription = new Subscription('foo@example.co.uk', 1)

    def "should add subscription when confirmation link is opened"() {
        given: "a confirmation URL"
        def confirmationUrl = prepareConfirmationUrl()

        when: "user opens the link"
        def response = httpClient.execute(new HttpGet(confirmationUrl))

        then: "user is redirected to 'Confirmed' page"
        response.getStatusLine().statusCode == 302
        response.getFirstHeader('Location').value.contains("/confirmed")

        then: "database contains subscription entry for the user"
        def count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM subscription " +
                "WHERE email = :email AND location_id = :locationId",
                new BeanPropertySqlParameterSource(subscription),
                Integer.class)
        count == 1
    }

    def "should redirect from confirmation link to 'Already subscribed' page when subscriber is already present in database"() {
        given: "a confirmation URL and a prefilled database with a subscriber"
        //TODO make it OR IGNORE
        jdbcTemplate.update("INSERT OR IGNORE INTO subscription (email, location_id) " +
                "VALUES (:email, :locationId)",
                new BeanPropertySqlParameterSource(subscription))

        String confirmationUrl = prepareConfirmationUrl()

        when: "user opens the link"
        def response = httpClient.execute(new HttpGet(confirmationUrl))

        then: "user is redirected to 'Already subscribed' page"
        response.getStatusLine().statusCode == 302
        response.getFirstHeader('Location').value.contains("/already_subscribed")
    }

    private String prepareConfirmationUrl() {
        def identity = subscription.toIdentity()
        def encryptedIdentity = crypto.identityEncryptor().encrypt(identity)
        def confirmationToken = crypto.subscribeTokenEncoder().encode(identity)
        def confirmationUrl = "http://localhost:$port/confirm?i=$encryptedIdentity&t=$confirmationToken"
        return confirmationUrl
    }

}