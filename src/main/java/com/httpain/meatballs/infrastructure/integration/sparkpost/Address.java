package com.httpain.meatballs.infrastructure.integration.sparkpost;

import lombok.Value;

@Value
class Address {
    String email;
}
