package com.httpain.meatballs.job;

import com.httpain.meatballs.feature.newsletter.NewsletterService;
import com.httpain.meatballs.infrastructure.storage.LocationRepository;

public class NewsletterJob {

    private final LocationRepository locationRepository;
    private final NewsletterService newsletterService;

    public NewsletterJob(LocationRepository locationRepository, NewsletterService newsletterService) {
        this.locationRepository = locationRepository;
        this.newsletterService = newsletterService;
    }

    public void foo() {
        locationRepository.findAll().forEach(newsletterService::sendNewsletterForLocation);
    }
}
