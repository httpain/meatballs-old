package com.httpain.meatballs.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Email {
    String sender;
    String subject;
    String htmlContent;
}
