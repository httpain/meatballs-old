package com.httpain.meatballs.model;

import lombok.Value;

@Value
public class Location {
    long id;
    String name;
    String hostname;
    String newItemsPath;
}
